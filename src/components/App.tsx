import * as React from 'react';

class App extends React.PureComponent {
    render() {
        return (
            <div>
                <h1 className="d-flex justify-content-center">Some text...</h1>
                <div className="d-flex justify-content-center">
                    <img src="img/1.jpg" alt="Some corgi" />
                </div>
            </div>
        );
    }
}

export default App;
