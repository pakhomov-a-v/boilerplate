import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './scss/index.scss';

import App from './components/App';

/* global document */
ReactDOM.render(<App />, document.getElementById('root'));
