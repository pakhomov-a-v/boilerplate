module.exports = {
    parser: '@typescript-eslint/parser',
    extends: [
        'plugin:react/recommended',
        'prettier/@typescript-eslint',
        'plugin:@typescript-eslint/recommended',
        'eslint-config-airbnb',
    ],
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    rules: {
        '@typescript-eslint/no-var-requires': 0,
        'no-console': 0,
        'linebreak-style': 0,
        'global-require': 0,
        'import/no-extraneous-dependencies': 0,
        'import/extensions': 0,
        'import/no-unresolved': 0,
        indent: ['error', 4],
        'react/jsx-indent': ['error', 4],
        'react/jsx-indent-props': ['error', 4],
        'react/jsx-filename-extension': 0,
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
};
