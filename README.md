# Boilerplate React-Redux-Typescript #

Boilerplate for frontend development with React-Redux-Typescript.

Contain next additional features: 
    * Webpack
    * Bootstrap
    * SASS
    * ESLint with Airbnb presets
    * Prettier (need Prettier-plugin for properly work )
   

### Deploy boilerplate ###

    "npm run install"

    Install Prettier plugin

### Scripts ###

 * "start" - development mode
 * "build" - production mode

### Folders ###

 * Source folder ~/src
 * Build folder ~/dist
